;*********************************************************
; fdump.z80 - List contents of a file in hex and ASCII.
;*********************************************************

; size of local stack
STKSIZ  equ     040h

; ASCII characters
cr      equ     0dh         ; carriage return
lf      equ     0ah         ; line feed

; CP/M functions and addresses
wcon    equ     2           ; write (A) to CON:
pstr    equ     9           ; print string with $ terminator
open    equ     15          ; open existing file
close   equ     16          ; close an open file
readseq equ     20          ; read a buffer from an open file
setdma  equ     26          ; set I/O buffer address

drive   equ     4           ; current drive number {V2.2}
bdos    equ     5           ; system call entry (BDOS)
fcb1    equ     5ch         ; first FCB in CCP

ctcount equ     80h         ; address of the byte counter
comtail equ     ctcount+1   ; address of the command tail data
buffsiz equ     128         ; size of disk record

        org     100h        ; start of trans. program area

; start of the program
start:  ld      (cpmsp),sp  ; save the CP/M stack and
        ld      sp,stack    ;     set up local stack
        ld      a,(drive)   ; get current disk
        ld      (drvsav),a  ;     and save in "drsav"
        call    main        ; call the main code
        ld      a,(drvsav)  ; restore the current disk
        ld      (drive),a   ;
        ld      sp,(cpmsp)  ; restore the CP/M system stack
        ret                 ; return to CP/M

; Local storage
drvsav  db      0           ; current drive number
cpmsp   dw      0           ; CP/M stack pointer save

;*********************************************************
; Miscellaneous routines to print 4, 2 and 1 byte values in hex.
;*********************************************************

; print value in HL in hex on console
hl4hex: ld      a,h         ; show the high byte
        call    a2hex       ;
        ld      a,l         ; show the low byte
        call    a2hex       ;
        ret                 ;

; Output the hex value of the 8-bit number stored in A
a2hex:  push    af          ; save A
        rra                 ; get high nybble
        rra                 ;
        rra                 ;
        rra                 ;
        call    nybhex      ; show on console
        pop     af          ;
nybhex: and     0fh         ; get low nybble
        add     a,90h       ; convert binary to hex char
        daa                 ;
        adc     a,40h       ;
        daa                 ;
        call    chout       ;
        ret                 ;

;*********************************************************
; Hexdump 16 bytes of memory in one line on the console.
;     HL points to first byte of memory to dump
;     DE is offset of the first byte
;*********************************************************

delim1: db      ':  $'      ;
delim2: db      ' |$'       ;
delim3: db      '|',cr,lf,'$'

hexdmp: push    bc          ;
        push    de          ;
        push    hl          ;
        ex      de,hl       ;
        call    hl4hex      ; print offset of line dump
        ex      de,hl       ;
        push    hl          ;
        ld      de,delim1   ; print the first delimiter
        ld      c,pstr      ; 
        call    bdos        ;
        pop     hl          ; restore hl
                            ;
        push    hl          ; save copy of hL on stack
        ld      b,16        ; dump 16 bytes in this line
hloop:  ld      a,(hl)      ; next byte to dump
        inc     hl          ;
        call    a2hex       ; dump as 2 chars hex
        ld      a,' '       ; print delimiter
        call    chout       ;
        djnz    hloop       ;
                            ;
        ld      de,delim2   ; print delimiter
        ld      c,pstr      ;
        call    bdos        ;
                            ;
        pop     hl          ; restore HL to address of line start
        ld      b,16        ; 16 bytes, but printable ASCII
aloop:  ld      a,(hl)      ; get byte to print as ASCII
        inc     hl          ;
        cp      ' '         ; check if printable
        jr      c,notprn    ;
        cp      7fh         ;
        jr      c,okprn     ;
notprn: ld      a,'.'       ; unprintable, print '.'
okprn:  call    chout       ;
        djnz    aloop       ;
                            ;
        ld      de,delim3   ; print delimiter at end of line
        ld      c,pstr      ;
        call    bdos        ;
        pop     hl          ;
        pop     de          ;
        pop     bc          ;
        ret                 ;

;*********************************************************
; Common subroutines
;*********************************************************

; print message terminated by 0 to console
;     the stack points to the string
spmsg:  pop     de          ; get return address into hl
        ld      a,(de)      ; get next char from message
        inc     de          ; point to next char
        push    de          ; restore stack if end of message
        or      a,a         ; check if zero byte
        ret     z           ;     and return if so
        call    chout       ; else print char
        jr      spmsg       ;     and keep going

; Print char in A to console
chout:  push    bc          ; save registers
        push    de          ;
        push    hl          ;
        ld      c,wcon      ; select function
        ld      e,a         ; character to E
        call    bdos        ; output by CP/M
        pop     hl          ; restore registers
        pop     de          ;
        pop     bc          ;
        ret                 ;

;*********************************************************
; Start of actual code.
;*********************************************************

;----------------------------
; routine to print help text
;----------------------------
usage:  call    spmsg       ;
        db      cr,lf,'FDUMP - list file on console, hex+ASCII.',cr,lf
        db      cr,lf       ;
        db      'Usage: FDUMP FILENAME.TYP',cr,lf,0
        ret

;----------------------------
; main start point
;----------------------------
main:   ld      hl,ctcount  ; check that we have a filename
        ld      a,(hl)      ; get command tail length
        or      a,a         ; check for a filename
        jr      nz,gotf     ; jump if have file
        call    usage       ; otherwise help text
        ret                 ; and finish
                            ;
gotf:   ld      hl,fcb1     ; copy 15 bytes from "fcb1" to "xfcb"
        ld      de,xfcb     ; 
        ld      bc,15       ;
        ldir                ;
                            ;
        ld      de,xfcb     ; try to open file
        ld      c,open      ;
        call    bdos        ;
        cp      a,0ffh      ; check if OK
        jr      nz,fopen    ;
        call    spmsg       ;
        db      'File not found',cr,lf,0
        ret                 ;
                            ;
fopen:  ld      de,buff     ; set the I/O buffer
        ld      c,setdma    ;
        call    bdos        ;

        ld      de,0        ; set file offset
rdrec:  push    de          ; save DE offset
        ld      de,xfcb     ; read a record
        ld      c,readseq   ;
        call    bdos        ;
        pop     de          ; restore offset
        or      a,a         ; check return code
        jr      nz,goteof   ; jump if EOF
                            ;
        ld      hl,buff     ; point at record buffer
        ld      b,8         ; dump 8 lines of hex per record
loop:   call    hexdmp      ;
        push    bc          ; save BC
        ld      b,0         ;
        ld      c,16        ;
        add     hl,bc       ; bump HL by 16
        ex      de,hl       ;
        add     hl,bc       ; bump DE by 16
        ex      de,hl       ;
        pop     bc          ; get BC back, loop counter
        djnz    loop        ;
        jr      rdrec       ;

goteof: ld      de,xfcb     ; close the file
        ld      c,close     ;
        call    bdos        ;
        cp      a,0ffh      ; check if OK
        ret     nz          ;
        call    spmsg       ;
        db      'Close file error',cr,lf,0

; local FCB for reading file
xfcb:   ds      1           ; FCB disk number
        ds      8           ; FCB filename
        ds      3           ; FCB type
        ds      1           ; FCB extent
        ds      2           ; FCB reserved
        ds      1           ; FCB records used in this extent
        ds      16          ; FCB allocation blocks
        ds      1           ; FCB sequential record to read/write

; disk record buffer
buff:   ds      buffsiz     ; disk buffer

;*********************************************************
; Area for the stack.
;*********************************************************

        org     $+STKSIZ    ;
stack:                      ; local stack top

        end     start       ; start at "start"
