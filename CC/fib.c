#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned long fibx(int num, unsigned long a, unsigned long b)
{
    if (num == 0)
        return a;
    if (num == 1)
        return b;
    return fibx(num-1, b, a+b);
}

unsigned long fib(int num)
{
    return fibx(num, 0L, 1L);
}

int main(int argc, char *argv[])
{
    int num;

    if (argc < 2)
    {
        printf("Usage: fib N\n");
        return 1;
    }

    num = atoi(argv[1]);

    printf("fib(%d)=%lu\n", num, fib(num));
    return 0;
}


