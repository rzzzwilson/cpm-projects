#!/usr/bin/env python3
"""
Binary to CP/M file packager

Usage: filepack [-b n] [-c n] [-f n] [-p port] [-t n] [-u n] file1 [[file2] ... fileN]

Where:
    -b - Set port baud rate to n (default 115200)
    -c - Set command delay to n ms. (default 0)
    -f - Fill last 128 bytes sector with byte value n (default 0, no fill)
    -p - Send output to port (default stdout)
    -t - Set byte transfer delay to n ms. (default 0)
    -u - Select user n (default 0)
    file1...fileN - List of files to transfer
"""

# Binary to CP/M file packager
# Written for RC2014 computer by Marco Maccaferri
# Based on documentation by Grant Searle
#
# Modified for python 3 by r-w.

import sys
import glob
import ntpath
import getopt
from time import sleep
try:
    import serial
except ImportError:
    print('Need to do "workon work"!?')
    sys.exit(0)
import logger

log = logger.Log('filepack.log', logger.Log.DEBUG)


def encodeFile(filename, device):
    log(f"encodeFile: sending file '{filename}' to device {device}")

    # create End Of Line combination
    EOL = f'{chr(13)}{chr(10)}'

    name = ntpath.basename(filename).upper()

    with open(filename, 'rb') as f:
        # command to create file
        cmd = f'A:DNLD {name}{EOL}'
#        cmd = f'A:DOWNLOAD {name}{EOL}'
        log(f"Sending command: '{cmd}'")
        device.write(bytes(cmd, encoding='utf-8'))
        device.flush()
        sleep(cmd_delay)

        # user selection
        cmd = f'U{user}{EOL}'
        log(f"Sending command: '{cmd}'")
        device.write(bytes(cmd, encoding='utf-8'))
        device.flush()
        sleep(byte_delay)

        # start data stream
        log('sending data:')
        device.write(bytes(':', encoding='utf-8'))
        device.flush()
        sleep(cmd_delay)

        length = 0
        checksum = 0

        # data stream
        # wait  bit for first byte, for some reason
        # maybe DOWNLOAD.COM slow starting??
        sleep(1)
        log_buff = f'{length:04d}: '
        byte = f.read(1)
        while len(byte) == 1:
            data = f'{ord(byte):02X}'
            log_buff += f'{data} '
            if len(log_buff) >= 53:
                log(log_buff)
                log_buff = f'{length:04d}: '
            device.write(bytes(data, encoding='utf-8'))
            device.flush()
            length += 1
            checksum += ord(byte)
            byte = f.read(1)
            sleep(byte_delay);

        # fill last sector, if required
        if fill_ch != -1:
            fill_byte = f'{fill_ch:02X}'
            if length % 128:
                log(f'Filling last {128 - length % 128} bytes with {fill_byte}')
            while (length % 128):
                device.write(bytes(fill_byte, encoding='utf-8'))
                device.flush()
                sleep(byte_delay)
                length += 1
                checksum += fill_ch

        # end data stream and checksum
        data = f'>{length % 256:02X}{checksum % 256:02X}{EOL}'
        log(f"Checksum: '{data}'")
        device.write(bytes(data, encoding='utf-8'))
        device.flush()
        sleep(cmd_delay);

def usage(msg=None):
    if msg:
        print('*' * 64)
        print(msg)
        print('*' * 64)
    print(__doc__)


cmd_delay = 0
byte_delay = 0

fill_ch = -1
user = 0

port = None
baudrate = 115200

try:
    opts, args = getopt.getopt(sys.argv[1:], 'b:c:f:hp:t:u:')
    if len(args) == 0:
        usage()
        sys.exit(0)
except getopt.GetoptError as err:
    usage(err)
    sys.exit()

for o, a in opts:
    if o == '-b':
        baudrate = int(a)
    if o == '-c':
        cmd_delay = int(a) / 1000.0
    if o == '-f':
        fill_ch = int(a)
    if o == '-h':
        usage()
        sys.exit(0)
    if o == '-p':
        port = a
    if o == '-t':
        byte_delay = int(a) / 1000.0
    if o == '-u':
        user = int(a)

if port != None:
    device = serial.Serial()
    device.port = port
    device.baudrate = baudrate
    device.dtr = True
    device.open()
else:
    device = sys.stdout

for x in args:
    for fn in glob.glob(x):
        encodeFile(fn, device)

if port != None:
    device.close()
