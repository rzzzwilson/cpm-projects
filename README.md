# Simple CP/M example programs

This directory contains CP/M source code and executables that run
on the RC2014 Mini and CP/M upgrade machine.

https://rc2014.co.uk/

## Files

CPM-SAMP.ASM

    A simple "template" program showing how to save and restore
    the CP/M environment.  Got this off the 'net.

example1.z80

    Just print some text on the console.

example2.z80

    Shows how to access the command line data.

example2.z80

    Shows how to open and read a file given the name on the command line.

fdump.z80

    Program to dump data in a file as hex+ASCII.

cmp.z80

    A program to compare two files for equality.  Just does a byte by byte
    comparison and works on binary and text files.

cat.z80

    A program to list a text file on the console.


filepack.py
logger.py

    Python code to upload a file to the RC2014 machine.  Use like this:

    python3 ./filepack.py -c 2 -p /dev/cu.usbserial-00000000 -t 2 ZASMB.Z80

    to upload the ZASMB.Z80 to the RC2014.

lib

    A directory holding small utility Z80 assembler routines.

ZASMB.Z80
Z80ASM.COM
ZASM.COM

    Various assemblers for Z-80 code.
