#!/usr/bin/env python3
"""
Script to format 64MB or 128MB Compact Flash card and install CP/M.

Before running this, make sure you have placed a 64MB/128MB CF card
into the RC2014.

THIS WILL DESTROY ANY DATA ALREADY ON THE CARD!

Usage: format_cpm -p <port> -b <speed> <size>

where <port> is the USB port (eg, /dev/cu.usbserial-00000000, /dev/ttyUSB0),
      <speed> is the serial port speed (9600, 115200, etc),
and   <size> is either 64 or 128
"""

import sys
import os
import getopt
from time import sleep
try:
    import serial
except ImportError:
    print("Need to do install the 'pyserial' module.")
    sys.exit(0)


HexDataDir = 'z80sbcFiles/hexFiles'

Form128File = 'FORM128.HEX'
Form64File = 'FORM64.HEX'
Cpm22File = 'CPM22.HEX'
Cbios128File = 'CBIOS128.HEX'
Cbios64File = 'CBIOS64.HEX'
PutsysFile = 'PUTSYS.HEX'

ch_delay = 0.01
line_delay = 0.1


def flush_output(device):
    while resp := device.read(1):
        print(resp.decode('utf-8'), end='')

def send_line(line):
    flush_output(device)
    for ch in line:
        device.write(bytes(ch, encoding='utf-8'))
        device.flush()
        sleep(ch_delay)
    device.write(bytes('\r\n', encoding='utf-8'))
    device.flush()
    sleep(line_delay)
    flush_output(device)

def send_file(filename):
    with open(filename) as fd:
        for line in fd:
            line = line.strip()
            print(line)
            send_line(line)

def format_drive(device):
    # send the HEX data
    print(f'Writing {FormatHex} ...')
    send_file(FormatHex)

    flush_output(device)
    input("Press ENTER to format the CF drive.")
    print('G5000')
    device.write(bytes('G5000\r\n', encoding='utf-8'))
    flush_output(device)
    sleep(2)

def install_cpm(device):
    input("Press ENTER to install CP/M")
    # send the CP/M HEX data
    print(f'Writing {CpmHex} ...')
    send_file(CpmHex)

    # send the CBIOS HEX data
    print(f'Writing {CbiosHex} ...')
    send_file(CbiosHex)

    # send the PUTSYS HEX data
    print(f'Writing {PutsysHex} ...')
    send_file(PutsysHex)

    flush_output(device)
    input("Press ENTER to write CP/M to disk.")
    print('G5000')
    device.write(bytes('G5000\r\n', encoding='utf-8'))
    flush_output(device)
    sleep(2)

def usage(msg=None):
    if msg:
        print('*' * 64)
        print(msg)
        print('*' * 64)
    print(__doc__)

def main(device):
    # write a space to start monitor
    device.write(bytes(' ', encoding='utf-8'))
    flush_output(device)
    sleep(1)

    # format the 128MB drive
    format_drive(device)
    print('Drives formatted.')

    # install CP/M to drive B:
    install_cpm(device)
    print('CP/M installed.')

# port and speed values, filled in from the command line args
port = None
baudrate = None

try:
    opts, args = getopt.getopt(sys.argv[1:], 'hb:p:')
except getopt.GetoptError as err:
    usage(err)
    sys.exit()

for o, a in opts:
    if o == '-h':
        usage()
        sys.exit(0)
    elif o == '-b':
        try:
            baudrate = int(a)
        except ValueError:
            usage('Speed must be an integer value.')
            sys.exit(1)
    elif o == '-p':
        port = a

if port is None:
    usage('You must specify a port.')
    sys.exit(1)

# get and check the size argument
if len(args) != 1:
    usage('You must specify a valid size.')
    sys.exit(1)
size = args[0]
if not size in ('64', '128'):
    usage('You must specify a valid size.')
    sys.exit(1)

input('Plug in the RC2014 and press RESET now.\n')
print('This will TRASH any CF card in the RC2014.')
input(f'Are you absolutely sure you have a {size}MB card in the RC2014? ')
input('Check again!!')
print()

# set paths to the appropriate HEX files (by size)
CpmHex = os.path.join(HexDataDir, Cpm22File)
PutsysHex = os.path.join(HexDataDir, PutsysFile)
if size == '64':
    FormatHex = os.path.join(HexDataDir, Form64File)
    CbiosHex= os.path.join(HexDataDir, Cbios64File)
else:
    FormatHex = os.path.join(HexDataDir, Form128File)
    CbiosHex= os.path.join(HexDataDir, Cbios128File)

# OK, do the work
device = serial.Serial()
device.port = port
device.baudrate = baudrate
device.timeout = 0.02
device.dtr = True
device.open()

main(device)

device.close()
