CPMTOOLS and the RC2014
=======================

The procedures described here were first explained at:

    https://groups.google.com/g/rc2014-z80/c/VD22SEht0PY

Disk images
-----------

This directory contains raw images and disk subimages from the 64MB CF card
supplied with the RC2014 CP/M kit.

The file *rc2014.img* is the raw copy of the RC2014 CP/M CF card, produced
by attaching the CF card to a computer with an adapter and doing:

    sudo dd if=/dev/sda of=rc2014.img

The *rc2014_disk_X* files (X is one of "ABCDEFGH") are CP/M subimages of the
eight disks on the CF card.  Each is produced by:

    dd if=rc2014.img of=rc2014_disk_? bs=1M skip=?? count=8

where the *skip=* number is the offset to the start of the subimage:

+--------+--------+
| Disk   | skip=  |
+========+========+
|   A    |   0    |
+--------+--------+
|   B    |   8    |
+--------+--------+
|   C    |   16   |
+--------+--------+
|   D    |   24   |
+--------+--------+
|   E    |   32   |
+--------+--------+
|   F    |   40   |
+--------+--------+
|   G    |   48   |
+--------+--------+
|   H    |   56   |
+--------+--------+

cpmtools
--------

Use the *cpmtools* package to list, copy, delete, etc, CP/M files on
the disk subimages.  *cpmtools* are documented at:

    http://www.moria.de/~michael/cpmtools/

Working with subimages
----------------------

Use *cpmtools* to copy files to/from the disk subimages, delete files, etc.

To create a new raw image containing the changed disks, do:

    cat rc2014_disk_A rc2014_disk_B ... rc2014_disk_H >new.img
    sudo dd of=/dev/sda if=new.img

Cloning a working CP/M image
----------------------------

To create a working copy of the *rc2014.img* CP/M system on a new CF card,
do:

   sudo dd of=/dev/sda if=rc2014.img 
